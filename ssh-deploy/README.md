# CI: Deploy using SSHPASS

## Introduction

This is an example of continous deployment using Gitlab CI, Runnner server, target server, SSHPass, PM2 and simple Node application. 
This microservice only have 1 endpoint (`/`) just to test if the microservice can be accessed after deployment completed.

**Instruction !**
Replace words in square bracket [] with corresponding value


## Setup Runner Server
This server will be the server where gitlab runner recides. Go [here](https://docs.gitlab.com/runner/install/) to install gitlab runner on this server

This server must have open connection to target deployment server. Once the connection are opened you can test by using ssh command
```shell
    ssh [user]@[ip_target_host]
```
it will trigger interactive password input. input the user password. Once in you're good.

next you need to install SSHPASS.
```shell
    apt-get install sshpass
```
SSHPASS a utility designed for running ssh using the mode referred to as "keyboard-interactive" password authentication, but in non-interactive mode. 
SSHPASS require you to set SSH_PASS variable with user's password in target deployment server
```shell
    export SSH_PASS=$USER_PASS
```

Once its done you can check by using this command:
```shell
    sshpass -e ssh [user]@[ip_target_host] 'echo 1'     
```
if succes it will print 1 in your shell.

## Setup Target Deployment Server
This is the deployment server where we want to serve our microservice written in Node JS. To do that we need to ensure the server already has node environtment.
simply follow Node JS installation guide [here](https://nodejs.org/en/download/package-manager/). 

check if node and npm already installed use this command
```shell
    node -v && npm -v
```
if success it will print node and npm version respectively.

### PM2 
PM2 is a pcrocess manager, it will be usefull to start a Node application on background , monitor and stop it. 
```shell
    npm install pm2 -g
```

## Configuring Gitlab CI to deploy microservice
### Setup secret variable
as mentioned before, SSHPASS require SSHPASS variable to be filled with target server pass. Gitlab provide Secret variable so that those password will not be visible to public.

Go ahead to the [Gitlab CI/CD Setting](https://gitlab.com/masykur_marhendra/hello-swarm/-/settings/ci_cd) and expand `Variables` in the page. 
create USER_PASS variable with users password in its value
![cicd-config](../img/04-cicd-ssh-config.png)

in ```.gitlab-ci.yaml``` you can see:
```yaml
    stages:
    - deploy

    job3:
    stage: deploy
    script: 
        - export SSHPASS=$USER_PASS
        - sshpass -e scp -o stricthostkeychecking=no -r app [user]@[ip_target_host]:~
        - sshpass -e ssh [user]@[ip_target_host] 'source /etc/profile; pm2 stop app/index.js' 
        - sshpass -e ssh [user]@[ip_target_host] 'source /etc/profile; pm2 start app/index.js' 
    only:
        - runner-local
```

$USER_PASS is secret variable that we've already create before. 
```shell
    sshpass -e scp -o stricthostkeychecking=no -r app [user]@[ip_target_host]:~
```
this command basically transfer folder app to target server directory ~ using scp command
once app folder has been transfered to target server
```shell
    sshpass -e ssh [user]@[ip_target_host] 'source /etc/profile; pm2 stop app/index.js'
```
This command is to ssh to target server and stop same running microservice as we're going to deploy. Notice there are 2 commands there. First ```source /etc/profile;```, this command is used to tell sshpass to load all environtment variables in users profile. Second, ```pm2 stop app/index.js```. This command utilize pm2 to stop same running microservice as we're going to deploy. 
```shell
    sshpass -e ssh [user]@[ip_target_host] 'source /etc/profile; pm2 start app/index.js' 
```
same as before, only its for start node app service.

**Congratulations !**
Your App now should've been served in Target Server

```shell
    curl http://[ip_target_host]:3000/
```


see https://gitlab.com/AwesomeRei/gudang-pipeline/pipelines/143180742 for gitlab ci full result. 













