const express = require('express')
const bodyParser = require('body-parser')
const app = express()
const port = 3000

app.use(bodyParser.json())
app.use(
    bodyParser.urlencoded({
        extended: true,
    })
)

app.get('/', (request, response) => {
    response.json({ info: 'Node.js, Express, and Postgres API' })
})

app.get('/hello', (request, response) => {
    response.json({ info: 'Hello World' })
})
// app.get('/logs', db.getLogs)
// app.post('/log', db.createLog)

app.listen(port, () => console.log(`Example app listening at http://localhost:${port}`))