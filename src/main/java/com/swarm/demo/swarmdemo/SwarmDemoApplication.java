package com.swarm.demo.swarmdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SwarmDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(SwarmDemoApplication.class, args);
	}

}
