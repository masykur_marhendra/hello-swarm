package com.swarm.demo.swarmdemo;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import java.net.*;

@RestController
public class HealthController {
	
	@RequestMapping(method=RequestMethod.GET,path="/healthz", produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> healthz(){
        String hostname = "[unknown]";
        try {
            hostname =InetAddress.getLocalHost().getHostName();
        } catch (Exception ex){
            ex.printStackTrace();
        }
		return ResponseEntity.status(HttpStatus.OK).body("{\"status\":\"ok from machine address "+hostname+"\"}");
	}
}
