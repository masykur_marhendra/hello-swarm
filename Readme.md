# Hello Swarm

## Introduction

This is an example of microservice and its CI/CD pipeline to deploy to Docker Swarm in Play With Docker (PWD) environment. Microservice  only have 1 REST endpoint `/healthz` just to test if the microservice can be accessed after deployment completed

## How to Deploy to Docker Swarm

### Configuring Docker Swarm

If you don't have any server playground, you can go ahead to PWD environment and start creating Swarm Cluster. 

Go ahead to https://labs.play-with-docker.com/ and Click `Start` button, you need to login with your docker account
![pwd](img/01-pwd.png)

After logged in, create 2 new instance prior creating swarm cluster as follow:

![pwd2](img/02-pwd-2.png)

Select node1 to make this swarm cluster `manager` and create swarm cluster by typing

```shell
$ docker swarm init --advertise-addr eth0
```

If success, you will get following result.

``` shell
Swarm initialized: current node (n13rdbj67xkfo96202opxflcu) is now a manager.

To add a worker to this swarm, run the following command:

    docker swarm join --token SWMTKN-1-4mi2qfpal02s1kujzdjgzuyxf8yszjf9r6kbh99r2so018apxv-8jazv2cu2cg277ci6n9gfkfuw 192.168.0.23:2377

To add a manager to this swarm, run 'docker swarm join-token manager' and follow the instructions.
```

Go ahead and select `node2`. Copy the add worker command from the result  
```shell
docker swarm join --token SWMTKN-1-4mi2qfpal02s1kujzdjgzuyxf8yszjf9r6kbh99r2so018apxv-8jazv2cu2cg277ci6n9gfkfuw 192.168.0.23:2377
``` 
and type in the `  node2`. You will get following result

```
This node joined a swarm as a worker.
```

**Congratulations !**
Your Docker Swarm Cluster now is ready

> <font size=2>Notes: If you use your own servers / instances, you need to perform extra steps described in https://docs.docker.com/engine/swarm/swarm-tutorial/
>
> * Docker Engine 1.12 or later installed
> * You need two servers (Swarm Manager will also be Swarm Workers, but not the opposite):
>   * 1 for Swarm Manager
>   * 1 Swarm Workers
> * Open ports between servers
>   * TCP port 2377 for cluster management communications
>   * TCP and UDP port 7946 for communication among nodes
>   * UDP port 4789 for overlay network traffic
> </font>

### Configuring CI/CD (.gitlab-ci.yml)

Go ahead to the [Gitlab CI/CD Setting](https://gitlab.com/masykur_marhendra/hello-swarm/-/settings/ci_cd) and expand `Variables` in the page. As you can see there are several configuration reside in it.

![cicd-config](img/03-cicd-config.png)

| Variables | Description|
------------|------------|
|`DOCKER_HOST_VAR`| Configure this variable value with the IP address / Hostname of the Swarm Cluster Manager. Example URL if using PWD environment: `tcp://ip172-18-0-33-bqogr2iosm4g00br7iqg-2375.direct.labs.play-with-docker.com:2375`.|

**Congratulations !** 
Now you have done the configuration and your services is ready to be deployed in the Swarm Cluster.

Clone this repository

```git
git clone https://gitlab.com/masykur_marhendra/hello-swarm/
```

Make a new branch,, create your changes, and push the repository. After you push your commit, it will automatically triggers pipeline to be run

```git

git checkout -b <mybranch>
git add <file_changed>
git commit -m "<your message>"
git push origin <mybranch>
```

><font size=2>**Warning:**
> This demo example is not using secure communications when deploying to swarm. If you’re using a secured docker daemon socket (and you should!), you’ll need to provide the client certificates. You could do this using Gitlab’s Secret Variables feature.
>
> According to your level of protection, just add the variables with content of certs/keys and then create the neccessary layout for docker client during CI run. Also set DOCKER_TLS_VERIFY and use another socket (default 2376).
>
> Example (Variables: `TLSCACERT`, `TLSCERT`, `TLSKEY`)
> 
> ```yaml
> deploy-production:
>  stage: deploy
>  variables:
>    DOCKER_TLS_VERIFY: 1
>    SERVICE_NAME: hello-swarm
>  image: docker:latest
>  script:
>    - export DOCKER_HOST=$DOCKER_HOST_VAR
>    - docker login -u gitlab-ci-token -p "$CI_BUILD_TOKEN" "$CI_REGISTRY"
>    - mkdir -p ~/.docker
>    - echo "$TLSCACERT" > ~/.docker/ca.pem
>    - echo "$TLSCERT" > ~/.docker/cert.pem
>    - echo "$TLSKEY" > ~/.docker/key.pem
>    - docker stack deploy --with-registry-auth -c docker-stack.yml ${CI_PROJECT_NAMESPACE}-${CI_PROJECT_NAME}-${SERVICE_NAME}
>    - docker stack ls
>    - docker stack services masykur_marhendra-hello-swarm-hello-swarm
>  environment:
>    name: master
>    url: http://${CI_BUILD_REF_NAME}.example.com
>  only:
>    - master
> ```
>
> Last two commands defined in the `script` is to check if the services already deployed in swarm and see if it is running (indicated with Replicas number same with the desire replicas)
> </font>
